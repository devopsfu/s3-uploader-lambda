# s3-uploader-lambda

AWS Lambda Function to return a URL that can be used to upload a blob to AWS S3. 

There are a few components that need to be configured in order to make this solution work. The following flow happens for each file that gets uploaded.

1. User opens computer and visits uploader.example.com. This is a HTTP request to the S3 bucket that stores the React single page app or SPA.
2. User selects the file, adds their customer name and then clicks upload.
3. The browser then makes a request to API Gateway to hit AWS Lambda. Lambda evaluates the request and validates the data. If successful, it retruns a URL that can be used to upload a file to S3.
4. The browser get the response and kicks off a PUT request to the S3 bucket via the URL sent back in the response.

The URL that is sent back is only valid for a specific amount of time. In this code, its 60 seconds * 20. AWS Lambda requires a IAM service-role to be associated so it has permissions to the S3 bucket and the ability to execute when called by API Gateway. The least level of permissions provided to the AWS IAM role, the better (#0trust). The following sections will help you get all of this configured. Its not that bad :)

## Configuring the AWS S3 buckets
There are a few things to consider when setting up S3 buckets. First, the bucket that contains customer data should NEVER be public. This is a good/bad way to end up on the news or accidently expose customer data. 

Navigate over to the S3 section of AWS once logged in. Create a new bucket. The name, although arbitrary, should resemble the project for troubleshooting/discovery later on. I will refrence the first bucket as s3-uploader-customer-data from now on. Select your region and then proceed with the default setting of "Block All public access". We will not allow customers to access the bucket publicly, therefore, this should NEVER be public. Skip the advanced settings and create the bucket.

Now, lets repeat the process and create a second bucket and name it s3-uploader-webui. This will contain all the public react code and any images associated with the project. This bucket can be public and the objects inside are also public. To enable public files, uncheck the box for the "Block all public access" and accept the warning for this bucket ONLY. We will add the contents later in this document, but for now, we're all set on the S3 side.

## Configure IAM Policy
Before we can create a role, we need to create a policy that we can attach to the role. This policy will be restircted and only allow Lambda access to the specific S3 bucket we define and only the ability to PUT objects. This does not grant lambda read or delete access, only write/PUT.

Navigate to AWS IAM and click on policies. Click on create policy. Once there, click the JSON tab to enter JSON. Modify the JSON below to match your deployment and paste in the Create policy window. You can find your ARN address in the S3 bucket section if you elected to use a different name when creating the bucket.

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::s3-uploader-customer-data/"
        }
    ]
}
```

Click on Review policy and provide it with a name. I provide "s3-uploader-customer-data-put-policy" since this is specific to the S3 object in the name. Feel free to add a description for your teammates and then click create policy. With that created, we can move onto the roles.

## Configure IAM Roles
Now that the policy is created, we can create IAM roles specifcially for the S3 buckets we created. Head over to IAM in the AWS console. Once you're there, select Roles and create a new role. Select Lambda as the service. Create a new role. Select AWS service and then select Lambda. Click Next: Permissions. Here you will select the policy created just before this, if following the naming in this doc, it would be "s3-uploader-customer-data-put-policy". We also need to add the default AWS policy for Lambda, "AWSLambdaBasicExecutionRole" will allow our Lambda function to run. Without this, Lambda will not execute. Click Next: Tags and create/apply any appliciable tages. Click Next: Review and provide a name and description for your new role. I provided "lambda-s3-uploader" as the name for my role. Verify both permissions are attached and then create the role!

## Adding the Lambda function
Now that we have everything configured in S3 and IAM, we can bring it all together with Lambda. Lambda will take the inbound requests and validate them before sending back a one-time use URL that the client will use to upload the selcted file. The Lambda function is written in NodeJS and currently supports the latest NodeJS LTS which is 12.x. Lets create a new Lambda function for the uploader.

Navigate over to Lambda in the AWS console. Click on Create a function to get started. Select "Author from Scratch" and provide a name for your function, I used "s3-uploader" for my function. Make sure you select NodeJS 12.x for the Runtime. Finally, select the "Choose or create an execution role". Choose "Use an existing role" and selct your newly created role. If you followed the naming in the doc, the role name will be "lambda-s3-uploader".

We can now copy the contents of the index.js file in this repo to the function code for our new function. Make sure you save in the code editor and at the top before moving on.

We need to create a few environment variables so our client code can call the Lambda function. 

```
BUCKET = bucket name where we will store customer date (" s3-uploader-customer-data ")
CORS_HEADERS = Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token
CORS_METHODS = POST,OPTIONS
CORS_ORIGIN = uploader.example.com (or whatever URL the clients will be coming from. You can add * here as well for EVERYTHING)
SIZE = 2147483649 (used to specify the size in bits for the largest file that can be uploaded, 2GB in this case.)
```

Make sure you save the environment variables and the lambda function. 

## Configuring API Gateway
API Gateway is used to get requests to our Lambda functions. Although API Gateway can do much more than we're configuring it for, we just need to create a new endpoint to send requests to our Lambda function and back to the client. Navigate to API Gateway in the AWS console and create a new api. 

Choose the REST API (non private version) and click build. Choose "REST" for the protocol and then choose "New API" for Create new API. Add the API name and description. I choose to use "s3-uploader-apigw" for the name. You can choose regional for most deployments, however, if you need better performance, edge optimized may provide better performance.

Once the API is created, we need to add a few items to make it functional. Select Actions and click "Create Method". This will create an endpoint in the root of the current API. Select the POST option. Choose "Lambda Function" as the Integration type. Then choose to use "Lambda Proxy Intergration". Select the region that your AWS Lambda function exists in and provide the name of your lambda function. You can begin to type and select your function accordingly. If you're following along with this document, the function name is "s3-uploader". 

Now, we need to create an OPTIONS method as well since we will be calling this from outside the domain used to access the React app. Select the actions button, create a new method and choose the OPTIONS method. Select the "Mock" integration type and save. Select the "Method Response" link and configure the following headers under the first entry for status code 200.
```
Access-Control-Allow-Headers
Access-Control-Allow-Methods
Access-Control-Allow-Origin
```

Go back to the OPTIONS method and select "Integration Response". Add the mapping values under the first entry for reponse status code 200. Under the header mappings, make sure the following headers are configured.
```
Access-Control-Allow-Headers    'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
Access-Control-Allow-Methods    'POST,OPTIONS'
Access-Control-Allow-Origin     '*'
```
The allow-origin can and should be updated to reflect the URL or domain the request will be coming from. Leaving * configured is less secure, but can be used to troubleshoot access.

Now select the Actions button again and click "Deploy API". Here we can create a stage. Stages are used to deploy various versions of the API gateway for testing and production. I named the stage live and deployed. Once the deployment is complete, you will get the Invoke URL. This is the URL the client will make the Lambda request to. We need to configure a few policies before we can call this endpoint though.

## S3 Bucket Policies
These policies are used to make sure public access is not provided accidently. We need to configure a policy that allows our IAM role to make requests to the S3 bucket. This is similar to the IAM policy where we gave the Lambda function the permissions to call the S3 bucket, however, the S3 bucket still needs a policy to accept the request.

Navigate to S3 in the AWS console. Once there, select your private bucket. If you're following along with this document, the bucket name would be "s3-uploader-customer-data". Select permissions and then select Bucket Policy. Below is a sample bucket policy. Make sure you update the role name if you used something other than "lambda-s3-uploader" for your role name. Also, make sure you update the S3 bucket ARN if you used something other than "s3-uploader-customer-data" for your private bucket name.
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Allow Put Objects Only",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::121328132423:role/lambda-s3-uploader"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::s3-uploader-customer-data/*"
        }
    ]
}
```

We also need to add a CORS policy. CORS is used by modern browsers to ensure the website is only call resources it trusts. Click on CORS configuration and refrence the policy below. The * value for AllowedOrgin is not a best practice and should be updated to reflect the URL of your webapp. This can also be configured to match your domain.
https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

```
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```

## Testing API Gateway and Lambda
At this point, you should be able to make a HTTP POST request to your API gateway URL and get a URL back from Lambda. This can be done using something like Postman, Insomnia or CURL. To test everything, make a POST request with the following body:
```
{
	"customerName": "sovlabs",
	"size": 2048,
	"filename": "Thisworks.zip"
}
```

If successful, you should get back a status code of 200 and a URL in the body.
```
"https://bucket-name.s3.amazonaws.com/sovlabs/Thisworks.zip?AWSAccessKeyId=ASIAYVR3UKUX5TAB6B7P&Expires=1585102831&Signature=nVuAvgCTKWalnTaADJ0%2F50wU%2BMI%3D&x-amz-security-token=IQoJb3JpZ2luX2VjEHIaCXVzLWVhc3QtMSJHMEUCICBaEw5uDHQZDnvjyy2%2BpMCSwprBDAQgFFZMv2ZIj6t3AiEAr5qlNOAUMLMAVGB5BIM2r%2B2%2FnDupyUfWb9%2...95bp2Lj4CW6bKeWN%2BRwVA%3D"
```

You should also get some HTTP headers in the response as well. Check to make sure you got the correct values for your Allow-Control-Allow-Origin, Access-Control-Allow-Headers, and Access-Control-Allow-Methods. These values are configured as environment values in Lambda.

## Configure Public S3 Bucket for Serverless Website
We will configure a public bucket that we can use to access the React webapp once it is built. For now, we will just configure the bucket so that it can host a serverless website and the files needed to host the web frontend.

Navigate over to S3 in the AWS console. Once there, select your public S3 bucket, if following the naming in this document, the bucket name is "s3-uploader-webui". Select the "Permissions" tab and make sure the "Block all public access" checkbox is unchecked. Remember, this bucket will not contain any customer data and will only provide the WebApp files. 

Next, we need to enable the S3 bucket for serverless web hosting. Click the Properties tab and click "Static website hosting". Select "Use this bucket to host a website" and provide "index.html" for both "Index Document" and "Error Document". Click save and we're done with this bucket for now.

The react app can now be built and copied to this bucket. Once complete you can hit the website using the link provided in the "properties" => "static website hosting"

## Configure CloudFront SSL for the serverless website
By default, AWS does not provide HTTPs for serverless websites. Cloudfront can be used to provide SSL if applicable to the deployment. This section assumes you already have a certificate loaded in Certificate Manager or have the ability to upload a certificate there.

Navigate to CloudFront in the AWS console and create a new distribution. Select the web option and continue by clicking "Get Started".
provide the S3 serverless website URL for the "Static Website Hosting" field (it will automatically copy down to the Origin ID field as well).
Select Redirect HTTP to "HTTPS Viewer Protocol Policy"
Select GET, HEAD for the "HTTP Methods"
Note: Choose the correct price model for your deployment. The most afforable option is the "Use Only US, Canada and Europe" option.
Provide a valid CNAME for the cloudfront deployment. For instance, uploader.example.com (when using SSL, you need a certificate that will cover this, either a wildcard certificate or certificate that includes the domain configured)
Select "Custom SSL Certificate" and select the certificate from Certificate Manager
Everything else can remain default unless adjustments are required for your deployment.
Click "Create Distribution"

This will take anywhere from 5 to 15 minutes to complete. In the meantime, create the CNAME DNS entry with the information provide in the Distributions dashboard. A simple CNAME for uploader.example.com (or whatever CNAME you provided in the cloudfront config) CNAME entry pointing to the URL in the dashboard.

Once the deployment is complete, navigate to uploader.example.com and you should get a SSL certificate for your domain AND the S3 serverless website.

# Troubleshooting
There are a few components that can cause issues with the application. It is best to test the components seperately.

If you're getting an error when making the API call to API Gateway using something like Postman or Insomnia:
1. Check that Lambda is configured with the correct IAM role
2. Check that role is configured with the correct policies
3. Make sure the policy is configured for the correct
4. Check the S3 bucket policy to make sure the correct IAM role is specified and the correct resource is configured
5. Make sure the env variables for the Lambda function are configured correctly, specifically the bucket
6. Check the CloudWatch logs for the lambda function for errors
7. Status code 502s come from errors in Lambda, 422 if there is an error with the request body, 401 if the body contents are incorrect and a 200 if it can generate a URL.