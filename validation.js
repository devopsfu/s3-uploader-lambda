    // make sure the file extension is supported
    var fileRegex = /^(zip|7z|tar|tgz)$/;
    if (!fileRegex.test(body.filename.split('.').reverse()[0].toLowerCase())) {
        console.log('no match for the file extension, returning error');
        return {
            statusCode: 401,
            body: JSON.stringify("invaild file extension")
        }
    };

    //verify the case matches the correct format and then take the format for the case.
    var caseRegex = /^ssp-\d{2,5}|^\d{4}$/;
    if (caseRegex.test(body.supportCase.toLowerCase())) {
        console.log('support case regex match')
        body.supportCase = body.supportCase.toLowerCase().match(caseRegex)
    } else {
        return {
            statusCode: 401,
            body: JSON.stringify("invaild file support case")
        }
    }