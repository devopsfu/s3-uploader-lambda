/**
 * This is a AWS Lambda function that can be used to accept requests publicly and return a valid URL that can be used to upload
 * a file to AWS S3. Various validation can be added before return the link. The less publicly exposed documentation on what
 * is exposed will reduce the attack surface.
 */

const AWS = require('aws-sdk');
const s3 = new AWS.S3({ signatureVersion: "v4" });
// const https = require('https');

console.log('Starting Lambda Function');

/**
 * Given signature metadata, return the signed URL
 */
function createSignedUrl(signatureInfo) {
    return new Promise((resolve, reject) => {
        console.log('Attempting to retrieve a PUT URL');
        s3.getSignedUrl('putObject', signatureInfo, (err, url) => {
            if (err)
                return reject(err);
            resolve(url);
        });
    });
}


/**
 * Lambda entry point. event.body contains the data from API gateway in
 * string format.
 */
exports.handler = async (event) => {
    let body;
    try {
        console.log('INFO: Parse the data to JSON');
        body = JSON.parse(event.body);
    } catch (e) {
        console.log('ERROR: Cannot parse data in the request body.')
        return {
            statusCode: 422,
            body: JSON.stringify("cannot parse data"),
        };
    }

    //Make sure the file size is less than 2GB
    if (body.filesize < process.env.FILESIZE) {
        console.log('ERROR: File size is too large');
        return {
            statusCode: 401,
            body: JSON.string("invalid file size"),
            headers: {
                "Access-Control-Allow-Headers": process.env.CORS_HEADERS,
                "Access-Control-Allow-Methods": process.env.CORS_METHODS,
                "Access-Control-Allow-Origin": process.env.CORS_ORIGIN
            },
        };
    }

    // Validate incoming customer name is valid and within limit
    var customerNameRegex = /^[a-z\d\-_\s]+$/i;
    if (!customerNameRegex.test(body.customerName) || body.customerName.length > process.env.CUSTOMER_NAME_LENGTH) {
        return {
            statusCode: 401,
            body: JSON.stringify("invalid customer name"),
            headers: {
                "Access-Control-Allow-Headers": process.env.CORS_HEADERS,
                "Access-Control-Allow-Methods": process.env.CORS_METHODS,
                "Access-Control-Allow-Origin": process.env.CORS_ORIGIN
            },
        }
    }

    //validate incoming partner name is within limit and alphanumeric
    if (body.partnerName) {
        var partnerNameRegex = /^[a-z\d\-_\s]+$/i;
        if (!partnerNameRegex.test(body.partnerName) || body.partnerName.length > process.env.CUSTOMER_NAME_LENGTH) {
            return {
                statusCode: 401,
                body: JSON.stringify("invalid partner name"),
                headers: {
                    "Access-Control-Allow-Headers": process.env.CORS_HEADERS,
                    "Access-Control-Allow-Methods": process.env.CORS_METHODS,
                    "Access-Control-Allow-Origin": process.env.CORS_ORIGIN
                },
            }
        }
    }

    var fileRegex = /^(zip|7z|tar|tgz|json|txt|doc|docx)$/;
    if (!fileRegex.test(body.filename.split('.').reverse()[0].toLowerCase())) {
        console.log('no match for the file extension, returning error');
        return {
            statusCode: 401,
            body: JSON.stringify("invalid file type"),
            headers: {
                "Access-Control-Allow-Headers": process.env.CORS_HEADERS,
                "Access-Control-Allow-Methods": process.env.CORS_METHODS,
                "Access-Control-Allow-Origin": process.env.CORS_ORIGIN
            },
        }
    };

    let signatureInfo = {
        Bucket: process.env.BUCKET,
        Key: `${body.customerName}/${body.filename}`,
        Expires: 60 * 20
    };

    if (body.customerName && body.partnerName) signatureInfo.Key = `${body.customerName}-${body.partnerName}/${body.filename}`

    try {
        const url = await createSignedUrl(signatureInfo);
        console.log("SUCCESS: successfully created the url")
        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Headers": process.env.CORS_HEADERS,
                "Access-Control-Allow-Methods": process.env.CORS_METHODS,
                "Access-Control-Allow-Origin": process.env.CORS_ORIGIN
            },
            body: JSON.stringify(url),
        };
    }
    catch (e) {
        console.log('ERROR: could not create the url and return it to the client');
        console.log(e)
        if (typeof e === 'object') e = JSON.stringify(e);
        return {
            statusCode: 500,
            error: e
        };
    }
};